{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedStrings     #-}
{-# LANGUAGE TemplateHaskell       #-}

module Widgets.Matrix
    ( MatrixEdit
    , matrixEmpty
    , setRowIds
    , setColIds
    , handleMatrixEvent
    , matrixAttr
    , matrixFocusedAttr
    , renderMatrix
    , matrixContents
    , getRowId
    , getRowColId
    , setMatrixValue
    , setMatrix
    , deleteMatrixValue
    ) where


import           Brick              (AttrName, Named (getName), hBox, str, vBox,
                                     withAttr)
import           Brick.Types        (EventM, Size (Greedy), Widget (Widget),
                                     availHeightL, availWidthL, getContext,
                                     image, render)
import           Data.List          (intersperse)
import           Data.Map.Strict    (Map)
import qualified Data.Map.Strict    as Map (empty, insert, lookup)
import           Data.Monoid        ((<>))
import           Graphics.Vty       (Event (EvKey),
                                     Key (KDown, KLeft, KRight, KUp))
import           Graphics.Vty.Image (imageWidth)
import           Lens.Micro         ((%~), (&), (.~), (^.), _1, _2)
import           Lens.Micro.TH      (makeLenses)

data MatrixEdit a b c n = Matrix {
                                _rowIds          :: [a]
                              , _colIds          :: [b]
                              , _matrixContents  :: Map (a,b) c
                              , _matDrawContents :: c -> Widget n
                              , _matrixName      :: n
                              , _pos             :: (Int,Int)
                              }

makeLenses ''MatrixEdit


instance Named (MatrixEdit a b c n) n where
    getName = _matrixName


instance (Show a, Show b, Show c, Show n) => Show (MatrixEdit a b c n) where
    show m =
        concat [ "Matrix { "
               , "rowIds = " ++ show (m ^. rowIds)
               , "colIds = " ++ show (m ^. colIds)
               , "content = " ++ show (m ^. matrixContents)
               , ", editorName = " ++ show (m ^. matrixName)
               , ", pos = " ++ show (m ^. pos)
               , "}"
               ]

matrixEmpty :: n -> (c -> Widget n) -> MatrixEdit a b c n
matrixEmpty name draw = matrix [] [] Map.empty draw name (0, 0)

matrix :: [a] -> [b] -> Map (a, b) c -> (c -> Widget n) -> n -> (Int,Int) -> MatrixEdit a b c n
matrix = Matrix

setRowIds :: [a] -> MatrixEdit a b c n -> MatrixEdit a b c n
setRowIds ids mat =  mat & rowIds .~ ids

setColIds :: [b] -> MatrixEdit a b c n -> MatrixEdit a b c n
setColIds ids mat =  mat & colIds .~ ids

handleMatrixEvent :: Event -> MatrixEdit a b c n -> EventM n (MatrixEdit a b c n)
handleMatrixEvent e mat = let f = case e of
                                   EvKey KUp []    -> reverseRow
                                   EvKey KDown []  -> advanceRow
                                   EvKey KLeft []  -> reverseCol
                                   EvKey KRight [] -> advanceCol
                                   _               -> id
                           in return $ f mat
 where
   reverseRow m = m & (pos . _1) %~ \x -> max (x - 1) 0
   advanceRow m = m & (pos . _1) %~ \x -> min (x + 1) numberRows
   reverseCol m = m & (pos . _2) %~ \x -> max (x - 1) 0
   advanceCol m = m & (pos . _2) %~ \x -> min (x + 1) numberCols

   numberRows = length (mat ^. rowIds) - 1
   numberCols = length (mat ^. colIds) - 1

matrixAttr :: AttrName
matrixAttr = "matrix"

matrixFocusedAttr :: AttrName
matrixFocusedAttr = matrixAttr <> "focused"


renderMatrix :: (Ord a, Ord b, Show a, Show b) => Bool -> MatrixEdit a b c n -> Widget n
renderMatrix _ mat = Widget Greedy Greedy $ do
  c <- getContext

  let heightAv = c ^. availHeightL
  let widthAv = c ^. availWidthL

  let (x,y) = mat ^. pos

  let drawElem = maybe (str " ") (mat ^. matDrawContents)

  let attr r c  | r == (mat ^. rowIds) !! fst (mat ^. pos) && c == (mat ^. colIds) !! snd (mat ^. pos)  = matrixFocusedAttr
                | otherwise                                                                             = matrixAttr


  let makeCol cId = vBox . intersperse (str " ") . (str (show cId) :)
      makeRow = hBox . intersperse (str " ")

      widgetWidth w = imageWidth . image <$> render w

      subrange l xs = take l $ drop start xs
        where
          start = if length xs + 1 - (l `div` 2) >= x
                    then max 0 (x - (l `div` 2))
                    else length xs - l + 1

      firstCol = vBox $ (str " " :) $ (str " " :) $ map str $ intersperse " " $ map show $ subrange (heightAv `div` 2) $ mat ^. rowIds


  let drawnElems = makeRow $ firstCol : [ makeCol colId [ withAttr (attr rowId colId) $
                                                              drawElem $
                                                              Map.lookup (rowId,colId) $
                                                              mat ^. matrixContents
                                                          | rowId <- subrange (heightAv `div` 2) $ mat ^. rowIds]
                                          | colId <- mat ^. colIds]

  render drawnElems

getRowId :: MatrixEdit a b c n -> a
getRowId mat = (mat ^. rowIds) !! (mat ^. pos ^. _1)

getColId :: MatrixEdit a b c n -> b
getColId mat = (mat ^. colIds) !! (mat ^. pos ^. _2)

getRowColId :: MatrixEdit a b c n -> (a,b)
getRowColId mat = (getRowId mat, getColId mat)

setMatrixValue :: (Ord a, Ord b) => c -> MatrixEdit a b c n -> MatrixEdit a b c n
setMatrixValue v mat = mat & matrixContents %~ Map.insert (getRowColId mat) v


setMatrix :: Map (a,b) c -> MatrixEdit a b c n -> MatrixEdit a b c n
setMatrix cont mat = mat & matrixContents .~ cont


deleteMatrixValue :: (Ord a, Ord b, Monoid c) => MatrixEdit a b c n -> MatrixEdit a b c n
deleteMatrixValue = setMatrixValue mempty
