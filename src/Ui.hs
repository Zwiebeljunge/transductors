{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell   #-}

module Ui
    ( uiMain
    ) where

import           Brick                    (App (App), AttrMap, appAttrMap,
                                           appChooseCursor, appDraw,
                                           appHandleEvent, appStartEvent,
                                           attrMap, continue, defaultMain, halt,
                                           handleEventLensed, on,
                                           showFirstCursor, suspendAndResume)
import           Brick.Focus              (FocusRing, focusGetCurrent,
                                           focusNext, focusPrev, focusRing,
                                           withFocusRing)
import           Brick.Types              (BrickEvent, BrickEvent (VtyEvent),
                                           EventM, Next, Widget)
import           Brick.Widgets.Border     (borderWithLabel)
import           Brick.Widgets.Center     (center, hCenter, vCenter)
import           Brick.Widgets.Core       (hLimit, padAll, padLeftRight,
                                           padTopBottom, padTopBottom, str,
                                           vBox, vLimit, (<+>), (<=>))
import           Brick.Widgets.Dialog     (Dialog, buttonAttr,
                                           buttonSelectedAttr, dialog,
                                           dialogAttr, dialogSelection,
                                           handleDialogEvent, renderDialog)
import           Brick.Widgets.Edit       (Editor, editAttr, editFocusedAttr,
                                           handleEditorEvent, renderEditor)
import           Brick.Widgets.List       (List, handleListEvent, list,
                                           listAttr, listMoveTo, listReplace,
                                           listSelectedAttr,
                                           listSelectedElement,
                                           listSelectedFocusedAttr,
                                           listSelectedL, renderList)
import           Control.Arrow            (first)
import           Control.Lens.Tuple       (_1, _2, _3, _4, _5, _6, _7, _8)
import           Control.Monad            (filterM, void)
import           Control.Monad.IO.Class   (liftIO)
import           Data.Char                (isDigit, toLower)
import           Data.Foldable            (toList)
import qualified Data.Function            as F (on)
import           Data.List                (intercalate, intersperse, sortBy)
import           Data.List.Split          (splitOn)
import qualified Data.Map.Strict          as Map (assocs, filterWithKey, lookup,
                                                  map, mapKeys)
import           Data.Maybe               (fromMaybe, mapMaybe)
import           Data.Set                 (Set)
import qualified Data.Set                 as Set (delete, empty, filter,
                                                  fromList, insert, map, member,
                                                  null)
import qualified Data.Vector              as Vec (fromList)
import           Graphics.Vty             (Event (EvKey), Key (KBS, KBackTab, KChar, KDel, KEnter, KEsc),
                                           Modifier (MCtrl), black, defAttr,
                                           rgbColor, white)
import           Lens.Micro               hiding (_1, _2, _3, _4, _5)
import           Lens.Micro.TH            (makeLenses)
import           System.Directory         (doesDirectoryExist, doesFileExist,
                                           getCurrentDirectory, listDirectory)
import           System.FilePath          (makeValid, takeBaseName,
                                           takeExtension, (</>))
import qualified System.FilePath          as File ((<.>))

import           BrickUtils               (clearEditor, dropLastDirectory,
                                           dropPath, editContent, emptyEditor,
                                           emptyList, focusOn, focusSkip,
                                           handleAlphabetEditorEvent, isFocused,
                                           isLetterKey, listDeleteSelected,
                                           listDrawElement, listGetElem,
                                           listIndex, readMaybe,
                                           setEditorContent, whenNotEmpty,
                                           (<++>))
import           FST.AmoreParser          (loadAmoreAutomaton)
import           FST.AmorePrinter         (isValidAmoreName, saveAmoreAutomaton,
                                           validAmoreNameMessage)
import           FST.AutomatonFunctions   (loadAutomaton, replaceAlphabet,
                                           saveAutomaton)
import           FST.AutomatonTypes       (AmoreAutomaton, Automaton (alphabet))
import           FST.TransductorFunctions (loadTransductor, saveTransductor)
import           FST.TransductorTypes     (State (St, unState), Symbol (Eps, S),
                                           Transductor, mkTransductor)
import qualified FST.TransductorTypes     as Trans (finalStates, initialStates,
                                                    inputAlphabet,
                                                    outputAlphabet, states,
                                                    transitions)
import           FST.Types                (AppError, Error)
import           FST.Utils                (transDropOutput, trimTrans, (<.>))
import           Widgets.Matrix           (MatrixEdit, deleteMatrixValue,
                                           getRowColId, getRowId,
                                           handleMatrixEvent, matrixAttr,
                                           matrixContents, matrixEmpty,
                                           matrixFocusedAttr, renderMatrix,
                                           setColIds, setMatrix, setMatrixValue,
                                           setRowIds)

data Name = Aut | Trans | AutLoaded | TransLoaded | ExitDialog | FileBrowser | FileBrowserFileName | OverrideDialog | Create CreateMode | AlphabetMapping
  deriving (Eq, Ord, Show)

data CreateMode = TransName | StateNumber | Input | Output | Transitions | TransEntry
  deriving (Eq, Ord, Show, Enum)

data FileBrowserMode = LoadTrans | LoadAmore | LoadAut | SaveTrans | SaveAmore | SaveAut | Delete | Edit | ComposeAT | ComposeTT (Maybe Int) | Show | ConvertToDFA
  deriving (Eq, Ord, Show)

data DialogChoice = Yes | No
  deriving (Eq, Ord, Show)

type Initial = Integer
type Final = Integer

data AppState = State {
                        _focus       :: FocusRing Name
                      , _transFocus  :: List Name String
                      , _autFocus    :: List Name String
                      , _trans       :: List Name (String, Transductor Char Char)
                      , _auts        :: List Name (String, AmoreAutomaton)
                      , _exit        :: Dialog DialogChoice
                      , _fileBrowser :: ( FilePath
                                        , List Name FilePath
                                        , FileBrowserMode
                                        , Editor String Name
                                        , Dialog DialogChoice
                                        )
                      , _message     :: String
                      , _create :: (Editor String Name
                                  , Editor String Name
                                  , Editor String Name
                                  , Editor String Name
                                  , Set Initial
                                  , MatrixEdit Integer (Symbol Char) (Set (Symbol Char, Integer)) Name
                                  , Editor String Name
                                  , Set Final
                                  )
                      , _alphabetMapDialog ::(Maybe (String, AmoreAutomaton), Editor String Name)
                    }

makeLenses ''AppState

-- Lenses for easier readability

fileBrowserL :: Lens' AppState (FilePath, List Name FilePath, FileBrowserMode, Editor String Name, Dialog DialogChoice)
fileBrowserL = lens (^. fileBrowser) (\st fileB -> st & fileBrowser .~ fileB)

pathL :: Lens' AppState FilePath
pathL = lens ((^. _1) . (^. fileBrowserL)) (flip (fileBrowserL . _1 .~))

filesL :: Lens' AppState (List Name FilePath)
filesL = lens ((^. _2) . (^. fileBrowserL)) (flip (fileBrowserL . _2 .~))

browserModeL :: Lens' AppState FileBrowserMode
browserModeL = lens ((^. _3) . (^. fileBrowserL)) (flip (fileBrowserL . _3 .~))

fileNameL :: Lens' AppState (Editor String Name)
fileNameL = lens ((^. _4) . (^. fileBrowserL)) (flip (fileBrowserL . _4 .~))

browserDialogL :: Lens' AppState (Dialog DialogChoice)
browserDialogL = lens ((^. _5) . (^. fileBrowserL)) (flip (fileBrowserL . _5 .~))


createL :: Lens' AppState (Editor String Name, Editor String Name, Editor String Name, Editor String Name, Set Initial, MatrixEdit Integer (Symbol Char) (Set (Symbol Char, Integer)) Name, Editor String Name, Set Final)
createL = lens (^. create) (\st c -> st & create .~ c)

createNameL :: Lens' AppState (Editor String Name)
createNameL = lens ((^. _1) . (^. createL)) (flip (createL . _1 .~))

createStatesL :: Lens' AppState (Editor String Name)
createStatesL = lens ((^. _2) . (^. createL)) (flip (createL . _2 .~))

createInL :: Lens' AppState (Editor String Name)
createInL = lens ((^. _3) . (^. createL)) (flip (createL . _3 .~))

createOutL :: Lens' AppState (Editor String Name)
createOutL = lens ((^. _4) . (^. createL)) (flip (createL . _4 .~))

createInitsL :: Lens' AppState (Set Initial)
createInitsL = lens ((^. _5) . (^. createL)) (flip (createL . _5 .~))

createTransL :: Lens' AppState (MatrixEdit Integer (Symbol Char) (Set (Symbol Char, Integer)) Name)
createTransL = lens ((^. _6) . (^. createL)) (flip (createL . _6 .~))

createEntryL :: Lens' AppState (Editor String Name)
createEntryL = lens ((^. _7) . (^. createL)) (flip (createL . _7 .~))

createFinalsL :: Lens' AppState (Set Final)
createFinalsL = lens ((^. _8) . (^. createL)) (flip (createL . _8 .~))


alphMappingAutL :: Lens' AppState (Maybe (String, AmoreAutomaton))
alphMappingAutL = lens ((^. _1) . (^. alphabetMapDialog)) (flip (alphabetMapDialog . _1 .~))

alphMappingMapL :: Lens' AppState (Editor String Name)
alphMappingMapL = lens ((^. _2) . (^. alphabetMapDialog)) (flip (alphabetMapDialog . _2 .~))


drawUI :: AppState -> [Widget Name]
drawUI s = case focusedWidget s of
            Just ExitDialog           -> [ exitDialog ]
            Just OverrideDialog       -> [ overrideDialog ]
            Just FileBrowser          -> case s ^. browserModeL of
                                              SaveTrans -> [ center browser <=> hCenter fileNameEntry <=> hCenter messageWidget <=> instrWidget ]
                                              SaveAmore -> [ center browser <=> hCenter fileNameEntry <=> hCenter messageWidget <=> instrWidget ]
                                              SaveAut   -> [ center browser <=> hCenter fileNameEntry <=> hCenter messageWidget <=> instrWidget ]
                                              _         -> [ center browser <=> hCenter messageWidget <=> instrWidget ]
            Just FileBrowserFileName  -> [ center browser <=> hCenter fileNameEntry <=> hCenter messageWidget <=> instrWidget ]
            Just (Create w)           -> case w of
                                            Transitions -> transitionEntry
                                            TransEntry  -> transitionEntry
                                            _           -> [ createWidget  <=> hCenter messageWidget <=> instrWidget ]
            Just AlphabetMapping      -> mappingDialog
            _                         -> [ menu <+> loaded <=> hCenter messageWidget <=> instrWidget ]
    where
        mappingDialog = [center $ str "Enter mapping for the automaton alphabet:"
                          <=> padTopBottom 2 (hLimit 40 (withFocusRing (s ^. focus) renderEditor (s ^. alphMappingMapL)))]

        transitionEntry = [ vCenter $ hCenter (padTopBottom 1 transitionsWidget <=> padTopBottom 1 initals <=> padTopBottom 1 finals <=> padTopBottom 1 transEntryField) <=> hCenter messageWidget <=> instrWidget ]

        instrWidget = hCenter $ padTopBottom 2 $ case focusedWidget s of
                        Just FileBrowser -> padLeftRight 5 (str "arrow keys: navigate" <=> str "enter: select") <+> (str "backspace: parent directory" <=> str "esc: exit")
                        Just (Create Transitions) -> padLeftRight 5 (str "arrow keys: navigate" <=> str "enter: select") <+> (str "i: toggle inital state" <=> str "esc: exit") <+> padLeftRight 5 (str "f: toggle final state")
                        Just (Create TransEntry)  -> hCenter (str "Enter transitions symbol : state") <=>
                                                            str " " <=>
                                                      hCenter (padLeftRight 5 (str "arrow keys: navigate" <=> str "enter: select") <+> (str "tab: switch menu" <=> str "esc: exit"))
                        Just (Create _) -> padLeftRight 5 (str "enter: move to next field" <=> str "back tab: last field") <+> str "esc: exit"
                        _               -> padLeftRight 5 (str "arrow keys: navigate" <=> str "enter: select") <+> (str "tab: switch menu" <=> str "esc: exit")
        transitionsWidget = withFocusRing  (s ^. focus) renderMatrix (s ^. createTransL)

        transEntryField = str "Enter transition: " <+> hLimit 20 (withFocusRing (s ^. focus) renderEditor (s ^. createEntryL))

        initals = str "Inital states: " <+> str (head $ setToList show $ s ^. createInitsL)
        finals = str "Final states: " <+> str (head $ setToList show $ s ^. createFinalsL)

        createWidget = hCenter $ vCenter $ padAll 5 (str "Create new transducer") <=>
                                padTopBottom 1 (str "Enter transducer name " <+> hLimit 20 (withFocusRing (s ^. focus) renderEditor (s ^. createNameL))) <=>
                                padTopBottom 1 (str "Enter number of states " <+> hLimit 10 (withFocusRing (s ^. focus) renderEditor (s ^. createStatesL))) <=>
                                padTopBottom 1 (str "Enter input alphabet " <+> hLimit 10 (withFocusRing (s ^. focus) renderEditor (s ^. createInL))) <=>
                                padTopBottom 1 (str "Enter output alphabet " <+> hLimit 10 (withFocusRing (s ^. focus) renderEditor (s ^. createOutL)))

        messageWidget = s ^. message & str

        exitDialog = renderDialog (s ^. exit) $ hCenter $ padAll 1 $ str "Are you sure, you want to close the application?"

        overrideDialog = renderDialog (s ^. browserDialogL) $ hCenter $ padAll 1 $ str "Are you sure, you want to override an existing file?"

        fileNameEntry = padTopBottom 1 $ str "Enter filename: " <+> hLimit 20 (withFocusRing (s ^. focus) renderEditor (s ^. fileNameL))


        browser = borderWithLabel (s ^. pathL & (str . reverse . take 45 . reverse)) $
              hLimit 50 $
              vLimit 20 $
              renderList listDrawElement True (s ^. filesL)

        transMenu = borderWithLabel (str "Transducer Menu") $
              hLimit 20 $
              vLimit 10 $
              renderList listDrawElement (isFocused Trans $ s ^. focus) (_transFocus s)
        autMenu = borderWithLabel (str "Automaton Menu") $
              hLimit 20 $
              vLimit 10 $
              renderList listDrawElement (isFocused Aut $ s ^. focus) (_autFocus s)
        transLoaded = borderWithLabel (str "Transducers") $
              hLimit 20 $
              vLimit 10 $
              renderList listDrawElement (isFocused TransLoaded $ s ^. focus) $
              fmap fst (_trans s)
        autLoaded = borderWithLabel (str "Automata") $
              hLimit 20 $
              vLimit 10 $
              renderList listDrawElement (isFocused AutLoaded $ s ^. focus) $
              fmap fst (_auts s)
        menu = vCenter $ vBox [ hCenter transMenu
                              , hCenter autMenu
                              ]
        loaded = vCenter $ vBox [ hCenter transLoaded
                                , hCenter autLoaded
                                ]

focusedWidget :: AppState -> Maybe Name
focusedWidget s = focusGetCurrent (s ^. focus)

appEvent :: AppState -> BrickEvent Name e -> EventM Name (Next AppState)
appEvent s (VtyEvent e) = case (e, focusedWidget s) of
        (EvKey KEnter [], Just Trans)       -> case s ^. transFocus ^. listSelectedL of
                Just 0 -> continue $ showWidget (Create TransName) s
                Just 1 -> continue =<< showMessage "Select a transducer file to load" . showWidget FileBrowser <$>
                                        liftIO (updateDirectory (s & setBrowserMode LoadTrans))
                Just 2 -> continue $ showWhenNoError (emptyList trans)
                                                      "No transducer loaded"
                                                      (showMessage "Select transducer to save" .
                                                        showWidget TransLoaded .
                                                        setBrowserMode SaveTrans)
                                                      s
                Just 3 -> continue $ showWhenNoError (emptyList trans)
                                                    "No transducer loaded"
                                                    (showMessage "Select transducer to edit" .
                                                      showWidget TransLoaded .
                                                      setBrowserMode Edit)
                                                      s
                Just 4 -> continue $ showWhenNoError (emptyList trans)
                                                      "No transducer loaded"
                                                      (showMessage "Select transducer to delete" .
                                                        showWidget TransLoaded .
                                                        setBrowserMode Delete)
                                                      s
                Just 5 -> continue $ showWhenNoError (emptyList trans)
                                                      "No tranducers loaded"
                                                      (showMessage "Select first transducer to compose" .
                                                        showWidget TransLoaded .
                                                        setBrowserMode (ComposeTT Nothing))
                                                      s
                Just 6 -> continue $ showWhenNoError (emptyList trans)
                                                      "No tranducers loaded"
                                                      (showMessage "Select transducer to convert to DFA" .
                                                        showWidget TransLoaded .
                                                        setBrowserMode ConvertToDFA)
                                                      s
                Just 7 -> continue $ showWhenNoError (emptyList trans)
                                                      "No transducer loaded"
                                                      (showMessage "Select transducer to display" .
                                                        showWidget TransLoaded .
                                                        setBrowserMode Show)
                                                      s
                _      -> continue $ showMessage "Not yet supported" s
        (EvKey KEnter [], Just Aut)         -> case s ^. autFocus ^. listSelectedL of
                Just 0 -> continue =<< showMessage "Select a automaton file to load" . showWidget FileBrowser <$>
                                        liftIO (updateDirectory (s & setBrowserMode LoadAut))
                Just 1 -> continue $ showWhenNoError (emptyList auts)
                                                      "No automaton loaded"
                                                      (showMessage "Select automaton to save" .
                                                        showWidget AutLoaded .
                                                        setBrowserMode SaveAut)
                                                      s
                Just 2 -> continue =<< showMessage "Select a AMoRe file to load" . showWidget FileBrowser <$>
                                        liftIO (updateDirectory (s & setBrowserMode LoadAmore))
                Just 3 -> continue $ showWhenNoError (emptyList auts)
                                                      "No automaton loaded"
                                                      (showMessage "Select automaton to save to the AMoRe format" .
                                                        showWidget AutLoaded .
                                                        setBrowserMode SaveAmore)
                                                      s
                Just 4 -> continue $ showWhenNoError (emptyList auts)
                                                      "No automaton loaded"
                                                      (showMessage "Select automaton to delete" .
                                                        showWidget AutLoaded .
                                                        setBrowserMode Delete)
                                                      s
                Just 5 -> continue $ showWhenNoError (\st -> emptyList auts st || emptyList trans st)
                                                      "No automaton or tranductor loaded"
                                                      (showMessage "Select automaton to compose" .
                                                        showWidget AutLoaded .
                                                        setBrowserMode ComposeAT)
                                                      s
                Just 6 -> continue $ showWhenNoError (emptyList auts)
                                                      "No automaton loaded"
                                                      (showMessage "Select automaton to display" .
                                                        showWidget AutLoaded .
                                                        setBrowserMode Show)
                                                      s
                _      -> continue $ showMessage "Not yet supported" s
        (EvKey KEnter [], Just ExitDialog)  -> case dialogSelection $ s ^. exit of
                Just Yes -> halt s
                _        -> continue $ showWidget Trans s
        (EvKey KEnter [], Just OverrideDialog) -> case dialogSelection $ s ^. browserDialogL of
                                                    Just Yes  -> continue =<< saveLoaded False s
                                                    _         -> continue $ showWidget FileBrowser s
        (EvKey KEnter [], Just FileBrowser) -> continue . clearMessage =<< case currentSelectedPath s of
                Just newPath -> do
                                  isDir <- liftIO $ doesDirectoryExist newPath
                                  liftIO $ if isDir
                                              then s & newDirectory newPath
                                              else loadFile newPath s
                Nothing         -> return s
        (EvKey KEnter [], Just AutLoaded)   -> handleLoaded AutLoaded "Select location to save automaton" s
        (EvKey KEnter [], Just TransLoaded) -> handleLoaded TransLoaded "Select location to save transducer" s
        (EvKey KEnter [], Just (Create TransName))    -> continue $ s ^. createNameL & whenNotEmpty s (showWidget (Create StateNumber))
        (EvKey KEnter [], Just (Create StateNumber))  -> continue $
                                                          whenNotEmpty s (showWhenNoError ((== (Just 0 :: Maybe Int)) . readMaybe . editContent . (^. createStatesL))
                                                                                          "Transducer needs at least one state"
                                                                                          (showWidget (Create Input))) $
                                                          s ^. createStatesL
        (EvKey KEnter [], Just (Create Input))    -> continue $ s ^. createInL & whenNotEmpty s (showWidget (Create Output))
        (EvKey KEnter [], Just (Create Output))   -> continue $ showWidget (Create Transitions) $ trimTransitionsInitalsFinals $ setTransitionsMatrix s
        (EvKey KEnter [], Just (Create Transitions))  -> continue $ showWidget (Create TransEntry) $ s & createEntryL %~ setEditorContent (s ^. createTransL ^. matrixContents & maybe "" (concat . setToList showSymbolTuple)  . Map.lookup (getRowColId $ s ^. createTransL))
        (EvKey KEnter [], Just (Create TransEntry))   -> continue $
                                                          whenNotEmpty s (showWidget (Create Transitions) . (createEntryL %~ clearEditor) . insertTransitions) $
                                                          s ^. createEntryL
        (EvKey KEnter [], Just AlphabetMapping) -> continue $ showWidget Aut $ clearMappingDialog $ mapAlphabetAndInsert s
        (EvKey KEnter [], _)                -> continue s
        (EvKey KBS [], Just FileBrowser)  -> continue =<< liftIO (s & newDirectory (dropLastDirectory $ s ^. pathL))
        (EvKey KEsc [], Just ExitDialog)  -> continue $ clearMessage $ showWidget Trans s
        (EvKey KEsc [], Just OverrideDialog)  -> continue $ clearMessage $ showWidget FileBrowser s
        (EvKey KEsc [], Just FileBrowser) -> continue $ clearMessage $ showWidget Trans s
        (EvKey KEsc [], Just FileBrowserFileName) -> continue $ clearMessage $ showWidget FileBrowser s
        (EvKey KEsc [], Just TransLoaded) -> continue $ clearMessage $ showWidget Trans s
        (EvKey KEsc [], Just AutLoaded)   -> continue $ clearMessage $ showWidget Trans s
        (EvKey KEsc [], Just (Create Transitions))  -> continue $ clearMessage $ showWidget (Create TransName) s
        (EvKey KEsc [], Just (Create TransEntry))   -> continue $ clearMessage $ showWidget (Create Transitions) s
        (EvKey KEsc [], Just (Create _))   -> continue $ clearCreate $ clearMessage $ showWidget Trans s
        (EvKey KEsc [], _)                -> continue $ clearMessage $ showWidget ExitDialog s
        (EvKey (KChar '\t') [], Just ExitDialog)  -> continue =<< handleEventLensed s exit handleDialogEvent e
        (EvKey (KChar '\t') [], Just OverrideDialog)  -> continue =<< handleEventLensed s browserDialogL handleDialogEvent e
        (EvKey (KChar '\t') [], Just FileBrowser) -> case s ^. browserModeL of
                                                        SaveTrans -> continue $ showWidget FileBrowserFileName s
                                                        SaveAmore -> continue $ showWidget FileBrowserFileName s
                                                        SaveAut   -> continue $ showWidget FileBrowserFileName s
                                                        _         -> continue s
        (EvKey (KChar '\t') [], Just FileBrowserFileName) -> continue $ showWidget FileBrowser s
        (EvKey (KChar '\t') [], Just TransLoaded) -> continue s
        (EvKey (KChar '\t') [], Just AutLoaded)   -> continue s
        (EvKey (KChar '\t') [], Just (Create _))  -> continue s
        (EvKey (KChar '\t') [], _)                -> continue $ s & focus %~ focusSkip skippable focusNext
        (EvKey KBackTab [], Just ExitDialog)  -> continue =<< handleEventLensed s exit handleDialogEvent e
        (EvKey KBackTab [], Just OverrideDialog)  -> continue =<< handleEventLensed s browserDialogL handleDialogEvent e
        (EvKey KBackTab [], Just FileBrowser) -> continue s
        (EvKey KBackTab [], Just TransLoaded) -> continue s
        (EvKey KBackTab [], Just AutLoaded)   -> continue s
        (EvKey KBackTab [], Just (Create TransName))    -> continue s
        (EvKey KBackTab [], Just (Create StateNumber))    -> continue $ showWidget (Create TransName) s
        (EvKey KBackTab [], Just (Create Input))    -> continue $ showWidget (Create StateNumber) s
        (EvKey KBackTab [], Just (Create Output))    -> continue $ showWidget (Create Input) s
        (EvKey KBackTab [], Just (Create Transitions))  -> continue s
        (EvKey KBackTab [], Just (Create TransEntry))   -> continue s
        (EvKey KBackTab [], _)                -> continue $ s & focus %~ focusSkip skippable focusPrev
        (EvKey (KChar 'i') [], Just (Create Transitions)) -> continue $ toggleInital s
        (EvKey (KChar 'f') [], Just (Create Transitions)) -> continue $ toggleFinal s
        (EvKey (KChar 's') [MCtrl], Just FileBrowser) -> continue =<< saveLoaded True s
        (EvKey (KChar 's') [MCtrl], Just FileBrowserFileName) -> continue =<< saveLoaded True s
        (EvKey (KChar 's') [MCtrl], Just (Create Transitions)) -> continue $ if s ^. createInitsL & Set.null
                                                                              then showMessage "Select at least one state as initial" s
                                                                              else showWidget Trans $ clearCreate $ insertTransductor (createTransductor s) s
        (EvKey KDel [], Just (Create Transitions)) -> continue $ s & createTransL %~ deleteMatrixValue
        (_, Just Trans)       -> continue =<< handleEventLensed s transFocus handleListEvent e
        (_, Just Aut)         -> continue =<< handleEventLensed s autFocus handleListEvent e
        (_, Just TransLoaded) -> continue =<< handleEventLensed s trans handleListEvent e
        (_, Just AutLoaded)   -> continue =<< handleEventLensed s auts handleListEvent e
        (_, Just OverrideDialog)  -> continue =<< handleEventLensed s browserDialogL handleDialogEvent e
        (_, Just ExitDialog)  -> continue =<< handleEventLensed s exit handleDialogEvent e
        (_, Just (Create TransName))   -> continue =<< handleEventLensed s createNameL handleEditorEvent e
        (_, Just (Create Input))   -> continue =<< handleEventLensed s createInL handleAlphabetEditorEvent e
        (_, Just (Create Output))   -> continue =<< handleEventLensed s createOutL handleAlphabetEditorEvent e
        (_, Just (Create Transitions)) -> continue =<< handleEventLensed s createTransL handleMatrixEvent e
        (_, Just (Create TransEntry))  -> continue =<< handleEventLensed s createEntryL handleEditorEvent e
        (_, Just (Create StateNumber))  -> case e of
                  EvKey (KChar c) [] -> if isDigit c  then continue =<< handleEventLensed s createStatesL handleEditorEvent e
                                                      else continue $ showMessage "Only digits allowed" s
                  _                  -> continue =<< handleEventLensed s createStatesL handleEditorEvent e
        (EvKey (KChar x) _, Just FileBrowser) -> continue =<< if isLetterKey e
                                                  then do
                                                              let xs = s ^. filesL
                                                              let i = listIndex ((x ==) . toLower . head) xs
                                                              case i of
                                                                Just j  -> return $ s & filesL %~ listMoveTo j
                                                                Nothing -> return s
                                                  else handleEventLensed s filesL handleListEvent e
        (_, Just FileBrowser) -> continue =<< handleEventLensed s filesL handleListEvent e
        (_, Just FileBrowserFileName) -> continue =<< handleEventLensed s fileNameL handleEditorEvent e
        (_, Just AlphabetMapping) -> continue =<< handleEventLensed s alphMappingMapL handleAlphabetEditorEvent e
        (_, Nothing)          -> continue s
  where
    clearMappingDialog st = st  & alphMappingAutL .~ Nothing
                                & alphMappingMapL %~ clearEditor

    mapAlphabetAndInsert :: AppState -> AppState
    mapAlphabetAndInsert st = case st ^. alphMappingAutL of
                                Just nameAut -> case replaceAlphabet (Set.fromList $ parseAlphabet $ editContent $ st ^. alphMappingMapL) (snd nameAut) of
                                                  Just aut -> insertAutomaton (fst nameAut, aut) st
                                                  Nothing  -> st
                                Nothing      -> st


    saveLoaded :: Bool -> AppState -> EventM Name AppState
    saveLoaded checkOverride st | null $ editContent $ st ^. fileNameL = return $ showMessage "The filename cannot be empty" $ showWidget FileBrowserFileName st
                                | otherwise = case st ^. browserModeL of
                      SaveTrans -> do
                                    let path = makeValid $ st ^. pathL </> editContent (st ^. fileNameL) File.<.> ".tr"
                                    fileExists <- liftIO (doesFileExist path)
                                    if fileExists && checkOverride
                                        then return $ showWidget OverrideDialog st
                                        else saveTrans st
                      SaveAut   -> do
                                    let path = makeValid $ st ^. pathL </> editContent (st ^. fileNameL) File.<.> ".ar"
                                    fileExists <- liftIO (doesFileExist path)
                                    if fileExists && checkOverride
                                        then return $ showWidget OverrideDialog st
                                        else saveAut SaveAut st
                      SaveAmore -> do
                                    let fileName = editContent (st ^. fileNameL) File.<.> ".amr"
                                    if isValidAmoreName fileName
                                      then do
                                        let path = makeValid $ st ^. pathL </> fileName
                                        fileExists <- liftIO (doesFileExist path)
                                        if fileExists && checkOverride
                                            then return $ showWidget OverrideDialog st
                                            else saveAut SaveAmore st
                                      else return $ showMessage validAmoreNameMessage $ showWidget FileBrowser st
                      _         -> return s

    saveTrans :: AppState -> EventM Name AppState
    saveTrans st = case st ^. trans & ((snd <$>) . listSelectedElement) of
                    Just (_,tr) -> do
                        let path = makeValid $ st ^. pathL </> editContent (st ^. fileNameL) File.<.> ".tr"
                        liftIO $ saveTransductor path tr
                        return $ showMessage "Successfully saved" $ showWidget Trans st
                    _           -> return $ showMessage "Could not save, nothing selected" $ showWidget Trans st

    saveAut :: FileBrowserMode -> AppState -> EventM Name AppState
    saveAut saveMode st = case st ^. auts & ((snd <$>) . listSelectedElement) of
                            Just (_,aut) -> do
                                                  let path = makeValid $ st ^. pathL </> editContent (st ^. fileNameL)
                                                  liftIO $ case saveMode of
                                                              SaveAut   -> saveAutomaton (path File.<.> ".ar") aut
                                                              SaveAmore -> saveAmoreAutomaton (path File.<.> ".amr") aut
                                                              _         -> return ()
                                                  return $ showMessage "Successfully saved" $ showWidget Trans st
                            _               -> return $ showMessage "Could not save, nothing selected" $ showWidget Trans st

    setTransitionsMatrix :: AppState -> AppState
    setTransitionsMatrix st = st & createTransL %~ (setRowIds (transducerStates st) . setColIds (transducerAlphabet createInL st))

    transducerStates :: AppState -> [Integer]
    transducerStates st = let numSts = read $ editContent $ st ^. createStatesL
                            in take numSts [0..]

    transducerAlphabet :: Getting (Editor String n) AppState (Editor String n) -> AppState -> [Symbol Char]
    transducerAlphabet alphLens = (++ [Eps]) . toList . Set.fromList . map S . parseAlphabet . editContent . (^. alphLens)

    trimTransitionsInitalsFinals :: AppState -> AppState
    trimTransitionsInitalsFinals st =  let  sts = transducerStates st
                                            alph = transducerAlphabet createInL st
                                          in st & createTransL . matrixContents %~ flip restrictKeys (Set.fromList [(state,sy) | state <- sts, sy <- alph])
                                                & createInitsL %~ Set.filter (`elem` sts)
                                                & createFinalsL %~ Set.filter (`elem` sts)
      where
         -- TODO: should be replaced by restrictKeys from Data.Map.Strict as soon as containers-0.5.8 is on stack
        restrictKeys ms onlyKeys = Map.filterWithKey (\k _ -> Set.member k onlyKeys) ms

    parseAlphabet :: String -> String
    parseAlphabet ss = let cs = splitOn "," ss in
                          mapMaybe parseSymbol cs
      where
        parseSymbol [c] = Just c
        parseSymbol _   = Nothing


    populateCreate :: AppState -> AppState
    populateCreate st = st  & createNameL %~ setEditorContent name
                            & createStatesL %~ setEditorContent stateNum
                            & createInL %~ setEditorContent inp
                            & createOutL %~ setEditorContent outp
                            & createInitsL .~ initals
                            & createFinalsL .~ finals
                            & createTransL %~ setMatrix transTable
      where
        (_,(name,tr)) = fromMaybe (error "there should be a transducer seleced") $ st ^. trans & listSelectedElement

        stateNum    = show $ length $ Trans.states tr
        inp         = intersperse ',' $ toList $ Trans.inputAlphabet tr
        outp        = intersperse ',' $ toList $ Trans.outputAlphabet tr
        initals     = Set.map unState $ Trans.initialStates tr
        finals      = Set.map unState $ Trans.finalStates tr
        transTable  = Map.mapKeys (first unState) $ Map.map (Set.map (unState <$>)) $ Trans.transitions tr

    clearCreate :: AppState -> AppState
    clearCreate st = st & create .~ initalCreate

    insertTransductor :: (String, Transductor Char Char) -> AppState -> AppState
    insertTransductor tr st = st & trans %~ (<++>) tr

    insertAutomaton :: (String, AmoreAutomaton) -> AppState -> AppState
    insertAutomaton aut st = st & auts %~ (<++>) aut

    createTransductor :: AppState -> (String, Transductor Char Char)
    createTransductor st = (editContent $ st ^. createNameL,
                          trimTrans $ mkTransductor sts
                                                    inputA
                                                    outputA
                                                    initSt
                                                    finalSt
                                                    transList)
      where
        sts       = map St $ take (read $ editContent (st ^. createStatesL)) [0..]
        inputA    = parseAlphabet $ editContent $ st ^. createInL
        outputA   = parseAlphabet $ editContent $ st ^. createOutL
        initSt    = map St $ toList $ st ^. createInitsL
        finalSt    = map St $ toList $ st ^. createFinalsL
        transList = map (\(a,b,c,d) -> (St a,b,c,St d)) $ f $ Map.assocs $ Map.map toList $ st ^. createTransL ^. matrixContents

        f :: [((a,b),[(c,d)])] -> [(a,b,c,d)]
        f = concatMap (\((x,y),z) -> [(x,y,v,w) | (v,w) <- z])

    toggleInital :: AppState -> AppState
    toggleInital st = if Set.member (getRowId $ st ^. createTransL) $ st ^. createInitsL
                        then removeInital
                        else insertInital
      where
        insertInital = st & createInitsL %~ Set.insert (getRowId $ st ^. createTransL)
        removeInital = st & createInitsL %~ Set.delete (getRowId $ st ^. createTransL)

    toggleFinal :: AppState -> AppState
    toggleFinal st = if Set.member (getRowId $ st ^. createTransL) $ st ^. createFinalsL
                        then removeFinal
                        else insertFinal
      where
        insertFinal = st & createFinalsL %~ Set.insert (getRowId $ st ^. createTransL)
        removeFinal = st & createFinalsL %~ Set.delete (getRowId $ st ^. createTransL)


    mkTransitions :: String -> Set (Symbol Char, Integer)
    mkTransitions = Set.fromList . mapMaybe parseTransitions . splitOn ","

    parseTransitions :: String -> Maybe (Symbol Char, Integer)
    parseTransitions tr = case splitOn ":" tr of
                            ["eps",y] -> case readMaybe y of
                                            Just i  -> Just (Eps, i)
                                            Nothing -> Nothing
                            ["Eps",y] -> case readMaybe y of
                                            Just i  -> Just (Eps, i)
                                            Nothing -> Nothing
                            [[x],y]   -> case readMaybe y of
                                            Just i  -> Just (S x, i)
                                            Nothing -> Nothing
                            _         -> Nothing

    filterTransitions :: AppState -> Set (Symbol Char, Integer) -> Set (Symbol Char, Integer)
    filterTransitions st = Set.filter (\(sym, i) -> i `elem` transducerStates st && sym `elem` transducerAlphabet createOutL st)

    insertTransitions ::  AppState -> AppState
    insertTransitions st = st & createTransL %~ setMatrixValue (filterTransitions st $ mkTransitions $ editContent $ st ^. createEntryL)

    handleLoaded :: Name -> String -> AppState -> EventM Name (Next AppState)
    handleLoaded wid msg st = case s ^. browserModeL of
            ComposeAT -> continue $ case wid of
                            AutLoaded   -> showWidget TransLoaded $ showMessage "Select transducer to compose" st
                            TransLoaded -> showWidget Trans $ insertComposition st
                            _           -> error "Either TransLoaded or AutLoaded should be selected"
            ComposeTT Nothing   -> continue $
                                    showMessage "Select second transducer to compose" $
                                    showWidget TransLoaded $
                                    setBrowserMode (ComposeTT (st ^. trans . listSelectedL)) st
            ComposeTT (Just _)  -> continue $ showWidget Trans $ insertComposition st
            Delete  -> continue $ deleteFrom wid st
            Edit    -> continue $ showWidget (Create TransName) $ populateCreate s
            Show    -> case wid of
                          AutLoaded   -> showExternal msg auts $ showWidget Aut st
                          TransLoaded -> showExternal msg trans $ showWidget Trans st
                          _           -> error "Either TransLoaded or AutLoaded should be selected"
            ConvertToDFA -> case st ^. trans & listSelectedElement of
                              Just (_,(name,trans))  -> continue $ showWidget Trans $ insertAutomaton ("NFA: " ++ name,transDropOutput trans) st
                              Nothing   -> continue st
            _       -> continue =<< (showMessage msg . showWidget FileBrowser . updateFileBrowser)
                                                        <$> liftIO (updateDirectory st)

    updateFileBrowser :: AppState -> AppState
    updateFileBrowser st = case st ^. browserModeL of
                              SaveAut   -> setBrowserMode SaveAut st
                              SaveTrans -> setBrowserMode SaveTrans st
                              SaveAmore -> setBrowserMode SaveAmore st
                              _         -> st


    insertComposition :: AppState -> AppState
    insertComposition st = case s ^. browserModeL of
                            ComposeAT -> showWidget Aut $ case composeAT st of
                                            Right res -> showMessage "Successfully composed transducer with automaton" $ insertAutomaton res st
                                            Left err  -> showError err st
                            ComposeTT (Just i) -> showWidget Trans $ case composeTT i st of
                                            Right res -> showMessage "Successfully composed transducers" $ insertTransductor res st
                                            Left err  -> showError err st
                            _         -> showMessage "Could not compose, not sure what to compose" st

    showError :: Error -> AppState -> AppState
    showError err = showMessage (show err)

    composeAT :: AppState -> AppError (String, AmoreAutomaton)
    composeAT st = case res of
                      Right newAut -> Right (newName, newAut)
                      Left x       -> Left x
      where
        newName = tName ++ " . " ++ aName
        res = aut <.> tr

        (_,(tName,tr)) = fromMaybe (error "There should be a transducer seleced") $ st ^. trans & listSelectedElement
        (_,(aName,aut)) = fromMaybe (error "There should be a automaton seleced") $ st ^. auts & listSelectedElement


    composeTT :: Int -> AppState -> AppError (String, Transductor Char Char)
    composeTT i st = case res of
                      Right newTrans -> Right (newName, newTrans)
                      Left x         -> Left x
      where
        newName = t1Name ++ " . " ++ t2Name
        res = tr1 <.> tr2

        (_,(t1Name,tr1)) = fromMaybe (error "There should be a transducer seleced") $ listSelectedElement $ st ^. trans
        (t2Name,tr2) = fromMaybe (error "There should be a transducer seleced") $ listGetElem i $ st ^. trans


    deleteFrom :: Name -> AppState -> AppState
    deleteFrom AutLoaded    st = showWidget Aut $ st & auts %~ listDeleteSelected
    deleteFrom TransLoaded  st = showWidget Trans $ st & trans %~ listDeleteSelected
    deleteFrom _            st = st

    showExternal :: Show b => String -> Getting (List n (a, b)) AppState (List n (a, b)) -> AppState -> EventM n (Next AppState)
    showExternal msg ls st = case st ^. ls & ((snd . snd <$>) . listSelectedElement) of
                                Just obj -> suspendAndResume $ do
                                                  print obj
                                                  putStrLn "To return to the application press Enter"
                                                  _ <- getLine
                                                  return st
                                _             -> continue $ showMessage msg st

    setBrowserMode :: FileBrowserMode -> AppState -> AppState
    setBrowserMode SaveTrans st = st & browserModeL .~ SaveTrans & fileNameL %~ setEditorContent (maybe "" (fst . snd) $ listSelectedElement $ st ^. trans)
    setBrowserMode SaveAmore st = st & browserModeL .~ SaveAmore & fileNameL %~ setEditorContent (maybe "" (fst . snd) $ listSelectedElement $ st ^. auts)
    setBrowserMode SaveAut  st  = st & browserModeL .~ SaveAut & fileNameL %~ setEditorContent (maybe "" (fst . snd) $ listSelectedElement $ st ^. auts)
    setBrowserMode mode st      = st & browserModeL .~ mode

    skippable = [ExitDialog, OverrideDialog, FileBrowser, FileBrowserFileName, TransLoaded, AutLoaded, Create TransName, Create StateNumber, Create Input, Create Output, Create Transitions, Create TransEntry, AlphabetMapping]

    clearMessage = showMessage " "

    showWhenNoError :: (AppState -> Bool) -> String -> (AppState -> AppState) -> AppState -> AppState
    showWhenNoError pError msg stFunc st  | pError $ stFunc st  = showMessage msg st
                                          | otherwise           = stFunc st

    showMessage :: String -> AppState -> AppState
    showMessage msg st = st & message .~ msg

    showWidget :: Name -> AppState -> AppState
    showWidget name st = clearMessage $ st & focus %~ focusOn name

    loadFile :: FilePath -> AppState -> IO AppState
    loadFile path st = do
                        let fileName = takeBaseName path
                        case st ^. browserModeL of
                          LoadTrans -> showWidget Trans . (\tr -> insertTransductor (fileName, tr) st) <$> loadTransductor path
                          LoadAut   -> showWidget Aut . (\aut -> insertAutomaton (fileName, aut) st) <$> loadAutomaton path
                          LoadAmore -> do
                                        loaded <- loadAmoreAutomaton path
                                        case loaded of
                                          Left err -> return $ showError err st
                                          Right (name, aut) -> return $ showWidget AlphabetMapping $
                                                                st  & alphMappingAutL .~ Just (name, aut)
                                                                    & alphMappingMapL %~ setEditorContent (intersperse ',' $ toList $ alphabet aut)
                          _         -> return st

    newDirectory :: FilePath -> AppState -> IO AppState
    newDirectory path st = updateDirectory $ st & pathL .~ path

    updateDirectory :: AppState -> IO AppState
    updateDirectory st = do
                            let path = st ^. pathL
                            newFiles <- map (path </>) . sortBy (compare `F.on` map toLower) . filter (('.' /=) . head) <$> listDirectory path
                            filteredFiles <- map dropPath <$> case st ^. browserModeL of
                                              LoadTrans -> applicableExtensions ".tr" newFiles
                                              LoadAmore -> applicableExtensions ".amr" newFiles
                                              LoadAut   -> applicableExtensions ".ar" newFiles
                                              _         -> filterM doesDirectoryExist newFiles
                            return $ st & filesL %~ listReplace (Vec.fromList filteredFiles)
                                                                            (Just 0)

    currentSelectedPath :: AppState -> Maybe FilePath
    currentSelectedPath st = case st ^. filesL & listSelectedElement of
                              Just (_, dir) -> Just $ st ^. pathL </> dir
                              _             -> Nothing

    applicableExtensions:: String -> [FilePath] -> IO [FilePath]
    applicableExtensions ext = filterM (\path -> (takeExtension path == ext ||) <$> doesDirectoryExist path)

appEvent s _ = continue s

initialState :: IO AppState
initialState = do
              directory <- getCurrentDirectory
              filesInCurrentDirectory <- listDirectory directory
              return State {
                        _focus = focusRing [Trans, Aut, TransLoaded, AutLoaded, ExitDialog, FileBrowser, FileBrowserFileName, OverrideDialog, Create TransName, Create StateNumber, Create Input, Create Output, Create Transitions, Create TransEntry, AlphabetMapping]
                      , _transFocus = list Trans (Vec.fromList transductorOptions) 1
                      , _autFocus = list Aut (Vec.fromList automatonOptions) 1
                      , _trans = list TransLoaded (Vec.fromList []) 1
                      , _auts = list AutLoaded (Vec.fromList []) 1
                      , _exit = dialog (Just "Exit Dialog") (Just (1,[("Yes", Yes),("No", No)])) 60
                      , _fileBrowser = (,,,,) directory (list FileBrowser (Vec.fromList filesInCurrentDirectory) 1) LoadTrans (emptyEditor FileBrowserFileName) (dialog (Just "Override Dialog") (Just (1,[("Yes", Yes),("No", No)])) 60)
                      , _message = " "
                      , _create = initalCreate
                      , _alphabetMapDialog = (Nothing, emptyEditor AlphabetMapping)
                    }

initalCreate :: (Editor String Name, Editor String Name, Editor String Name, Editor String Name, Set a, MatrixEdit b c (Set (Symbol Char, Integer)) Name, Editor String Name, Set d)
initalCreate = (emptyEditor (Create TransName)
              , emptyEditor (Create StateNumber)
              , emptyEditor (Create Input)
              , emptyEditor (Create Output)
              , Set.empty
              , matrixEmpty (Create Transitions) (\c -> str $ if Set.null c then " " else concat $ setToList showSymbolTuple c)
              , emptyEditor (Create TransEntry)
              , Set.empty
              )



showSymbolTuple :: Show a => (Symbol Char, a) -> String
showSymbolTuple (x,y) = concat [showSymbol x, ":", show y]
  where
    showSymbol Eps   = "Eps"
    showSymbol (S w) = [w]

setToList :: (a -> String) -> Set a -> [String]
setToList showF = (:[]) . intercalate "," . map showF .  toList

theMap :: AttrMap
theMap = attrMap defAttr
    [ (listAttr,                white `on` black)
    , (listSelectedAttr,        black `on` gray)
    , (listSelectedFocusedAttr, black `on` white)
    , (dialogAttr,              white `on` black)
    , (buttonAttr,              white `on` gray)
    , (buttonSelectedAttr,      black `on` white)
    , (editAttr,                white `on` black)
    , (editFocusedAttr,         black `on` white)
    , (matrixAttr,              white `on` black)
    , (matrixFocusedAttr,       black `on` white)
    ]
  where
    gray = rgbColor 100 100 (100 :: Int)

app :: App AppState e Name
app = App { appDraw = drawUI
          , appChooseCursor = showFirstCursor
          , appHandleEvent = appEvent
          , appStartEvent = return
          , appAttrMap = const theMap
          }

transductorOptions :: [String]
transductorOptions = ["Create"
                    , "Load"
                    , "Save"
                    , "Edit"
                    , "Delete"
                    , "Compose"
                    , "Convert to NFA"
                    , "Display"
                    ]

automatonOptions :: [String]
automatonOptions = [ "Load"
                    , "Save"
                    , "Load AMoRe"
                    , "Save AMoRe"
                    , "Delete"
                    , "Compose"
                    , "Display"
                    ]


uiMain :: IO ()
uiMain = initialState >>= void . defaultMain app
