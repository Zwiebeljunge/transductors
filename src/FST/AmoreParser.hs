{-# LANGUAGE OverloadedStrings #-}

module FST.AmoreParser
    ( loadAmoreAutomaton
    ) where

import           Text.ParserCombinators.Parsec (ParseError, Parser, anyChar,
                                                char, digit, lookAhead, many,
                                                many1, manyTill, newline,
                                                noneOf, parse, string, try,
                                                (<|>))

import           FST.AutomatonTypes            (AmoreAutomaton, mkAutomaton)
import           FST.TransductorTypes          (State (St), Symbol (..))
import           FST.Types                     (AppError, Error (ParserError))


type Name = String

loadAmoreAutomaton :: FilePath -> IO (AppError (Name, AmoreAutomaton))
loadAmoreAutomaton file = do
  input <- stripWhiteSpaces <$> readFile file
  let aut = parseAmoreFile input
  case aut of
    Left msg  -> return $ Left $ ParserError $ show msg
    Right val -> return $ Right val

parseAmoreFile :: String -> Either ParseError (Name, AmoreAutomaton)
parseAmoreFile = parse amoreFileParser "AMoRe File Parser"


amoreFileParser :: Parser (Name, AmoreAutomaton)
amoreFileParser = do
  manyTill anyChar (try $ string "NAME")
  many (noneOf "\n")
  newline
  name <- many1 (noneOf "\n")
  newline
  manyTill anyChar (lookAhead (try $ string "DETERMINISTIC FINITE AUTOMATON"))
  automaton <- try detAutomatonParser <|> do
                                            skip 2 line
                                            nonDetAutomatonParser
  many anyChar
  return (name, automaton)


detAutomatonParser :: Parser AmoreAutomaton
detAutomatonParser = do
  string "DETERMINISTIC FINITE AUTOMATON"
  newline
  skip 2 line
  states <- stateParser
  newline
  string "DFA: INITIAL STATE"
  initals <- many1 (St . read <$> withNewline number)
  newline
  line
  alph <- alphabetParser
  newline
  skip 2 line
  string "DFA: FINAL STATES"
  finals <- map fst . filter snd . zip states <$> many1 (withNewline amoreBool)
  newline
  string "DFA: TRANSFORMATION"
  transitions <- zipWith (\(x,y) z -> (x,y,z)) [(st,S a) | a <- alph, st <- states]
                    <$> many1 (St . read <$> withNewline number)
  newline
  line
  return $ mkAutomaton  states
                        alph
                        initals
                        finals
                        transitions


nonDetAutomatonParser :: Parser AmoreAutomaton
nonDetAutomatonParser = do
  string "NONDETERMINISTIC FINITE AUTOMATON"
  newline
  skip 2 line
  states <- stateParser
  newline
  line
  alph <- alphabetParser
  newline
  skip 3 line
  string "NFA: FINAL STATES"
  stateInfo <- zipWith (flip numberToStInfo) states . map read
                              <$> many1 (withNewline number)
  newline
  string "NFA: TRANSFORMATION"
  transitions <- concatMap (\((qi,a),qjs) -> map ((,,) qi a) qjs) . zip [(st,S a) | a <- alph, st <- states] . map (toTargetStates states)
                    <$> many1 (read <$> withNewline number)
  newline
  line
  return $ mkAutomaton  states
                        alph
                        (map (\(_,_,st) -> st) $
                          filter (\(b,_,_) -> b) stateInfo)
                        (map (\(_,_,st) -> st) $
                          filter (\(_,b,_) -> b) stateInfo)
                        transitions
    where
      numberToStInfo 0 = (,,) False False
      numberToStInfo 1 = (,,) False True
      numberToStInfo 2 = (,,) True  False
      numberToStInfo 3 = (,,) True  True
      numberToStInfo _ = error "illegal state info"

      toTargetStates sts = map fst . filter snd . zip sts . toBin

      toBin 0 = [False]
      toBin i = toEnum (i `mod` 2) : toBin (i `div` 2)


alphabetParser :: Parser String
alphabetParser = flip take ['a'..] . read <$> number

stateParser :: Parser [State]
stateParser = map St . enumFromTo 0 . read <$> number

number :: Parser String
number = many1 digit

withNewline :: Parser a -> Parser a
withNewline parser = try $ newline *> parser

line :: Parser ()
line = many (noneOf "\n") *> newline >> return ()

skip :: Integer -> Parser b -> Parser ()
skip 0 _ = return ()
skip n p = p *> skip (n-1) p

stripWhiteSpaces :: String -> String
stripWhiteSpaces = unlines . map (reverse . dropWhiteSpace . reverse . dropWhiteSpace) . lines
  where
    dropWhiteSpace = dropWhile (' ' ==)


amoreBool :: Parser Bool
amoreBool = (char 'T' >> return True) <|>
            (char 'F' >> return False)
