module FST.TransductorFunctions
    ( saveTransductor
    , loadTransductor
    ) where

import           FST.Json             (JSON, deserialize, serialize)
import           FST.TransductorTypes (Transductor)


saveTransductor :: (JSON a, JSON b, Ord a, Ord b, Show a, Show b, Read a, Read b) => FilePath -> Transductor a b -> IO ()
saveTransductor path = writeFile path . serialize

loadTransductor :: (JSON a, JSON b, Ord a, Ord b, Show a, Show b, Read a, Read b) => FilePath -> IO (Transductor a b)
loadTransductor path = deserialize <$> readFile path
