{-# LANGUAGE OverloadedStrings #-}

module FST.AmorePrinter
    ( amorePrinter
    , saveAmoreAutomaton
    , isValidAmoreName
    , validAmoreNameMessage
    ) where

import           Data.Char              (isAlphaNum)
import           Data.Foldable          (toList)
import           Data.List              (intercalate)
import           Data.Map               (Map)
import qualified Data.Map               as Map (lookup)
import           Data.Maybe             (maybe)
import qualified Data.Set               as Set (member)
import           System.FilePath        (stripExtension, takeBaseName)

import           FST.AutomatonFunctions (isDeterministic, isEpsilonFree,
                                         nfa2dfa)
import           FST.AutomatonTypes     (Automaton (..))
import           FST.TransductorTypes   (Symbol (..))


saveAmoreAutomaton :: Ord a => FilePath -> Automaton a b -> IO ()
saveAmoreAutomaton path = writeFile path . amorePrinter name . nfa2dfa
  where
    name = takeBaseName path


amorePrinter :: Ord a => String -> Automaton a b -> String
amorePrinter name aut = init $ unlines $
                            ["AMORE Language Save File VERSION 1.0",
                              "NAME ",
                              name,
                              ""] ++
                              header aut ++
                              deterministicPrinter aut ++
                              nonDeterministicPrinter aut ++
                              footer

deterministicPrinter :: Ord a => Automaton a b -> [String]
deterministicPrinter aut | isDeterministic aut =
                        [ "DETERMINISTIC FINITE AUTOMATON ",
                          "T",
                          "DFA: #STATES",
                          show (length (states aut) - 1),
                          "DFA: INITIAL STATE",
                          intercalate "\n" $
                            map show $
                            toList $
                            initialStates aut,
                          "DFA: ALPHABET",
                          show $ length $ alphabet aut,
                          "DFA: MINIMAL",
                          toAmoreBool False,
                          "DFA: FINAL STATES",
                          intercalate "\n" $
                            map (toAmoreBool . (`Set.member`
                                                    finalStates aut)) $
                            toList $
                            states aut,
                          "DFA: TRANSFORMATION",
                          intercalate "\n" $
                            map (show . head . toList) $ toListWithKeys (transitions aut) [(st,S a) | a <- toList $ alphabet aut,
                                                                                                      st <- toList $ states aut],
                          "DFA: END"
                        ]
                        | otherwise = ["DETERMINISTIC FINITE AUTOMATON ",
                                        "F"]



nonDeterministicPrinter :: Ord a => Automaton a b -> [String]
nonDeterministicPrinter aut | isDeterministic aut =
                                [ "NONDETERMINISTIC FINITE AUTOMATON ",
                                  "F"]
                            | otherwise =
                                [ "NONDETERMINISTIC FINITE AUTOMATON ",
                                  "T",
                                  "NFA: #STATES",
                                  show (length (states aut) - 1),
                                  "NFA: ALPHABET",
                                  show $ length $ alphabet aut,
                                  "NFA: EPSILON, MINIMAL",
                                  toAmoreBool $ not $ isEpsilonFree aut,
                                  toAmoreBool False,
                                  "NFA: FINAL STATES",
                                  intercalate "\n" $
                                    map (\s -> toAmoreFinalState (s `Set.member` initialStates aut,
                                                                  s `Set.member` finalStates aut)) $
                                      toList $ states aut,
                                  "NFA: TRANSFORMATION",
                                  intercalate "\n" $
                                    map (\sts -> show $ bin2Dec $ map (`Set.member` sts) $ toList $ states aut) $
                                      toListWithKeys (transitions aut) [(st,S a) |  a <- toList $ alphabet aut,
                                                                                    st <- toList $ states aut],
                                  "NFA: END"
                                  ]
  where
    toAmoreFinalState (False,False) = "0"
    toAmoreFinalState (False,True)  = "1"
    toAmoreFinalState (True,False)  = "2"
    toAmoreFinalState (True,True)   = "3"

    bin2Dec :: [Bool] -> Integer
    bin2Dec []     = 0
    bin2Dec (b:bs) = 2 * bin2Dec bs + if b then 1 else 0


toListWithKeys :: (Monoid v, Ord k) => Map k v -> [k] -> [v]
toListWithKeys m = concatMap (maybe [mempty] (:[]) . (`Map.lookup` m))

toAmoreBool :: Bool -> String
toAmoreBool True  = "T"
toAmoreBool False = "F"

header :: Ord a => Automaton a b -> [String]
header aut =
          ["ALPHABET ",
          show $ length $ alphabet aut,
          "INPUT",
          if isDeterministic aut then "1" else "2",
          "BOOLE FLAGS: (BREX,BDFA,BNFA,BENFA,BMON) ",
          "REGULAR EXPRESSION ",
          "F"]

footer :: [String]
footer = ["MONOID ",
          "F",
          "STARFREE EXPRESSION ",
          "F",
          "EMPTY,FULL,FOLU,SF,LOC.TESTABLE,DEFINITE,REV.D.,GEN.D.,DOTDEPTH,NILPOTENT",
          "U",
          "U",
          "U",
          "U",
          "U",
          "U",
          "U",
          "U",
          "U",
          "U",
          "U",
          "0"]

isValidAmoreName :: FilePath -> Bool
isValidAmoreName fileName = case stripExtension ".amr" fileName of
                          Just name -> (length name <= 9) && all isAlphaNum name
                          Nothing   -> False

validAmoreNameMessage :: String
validAmoreNameMessage = "valid AMoRe filenames: only letters and number, no longer than 9 characters"
