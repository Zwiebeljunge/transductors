module FST.AutomatonFunctions
    ( isDeterministic,
      isEpsilonFree,
      nfa2dfa,
      epsSuccessors,
      successorStates,
      saveAutomaton,
      loadAutomaton,
      mapAutomaton,
      replaceAlphabet
    ) where


import           Control.Arrow        (first)
import           Data.Foldable        (foldMap, toList)
import           Data.Map.Strict      (Map)
import qualified Data.Map.Strict      as Map (empty, foldrWithKey, insert, keys,
                                              lookup, map, mapKeys, member,
                                              singleton, unions, (!))
import           Data.Maybe           (fromMaybe)
import           Data.Set             (Set)
import qualified Data.Set             as Set (deleteFindMin, elemAt, empty,
                                              filter, findIndex, fromList,
                                              insert, map, member, null,
                                              singleton, size, union, unions)


import           FST.AutomatonTypes   (Automaton (..))
import           FST.Json
import           FST.TransductorTypes (State (..), Symbol (..))



saveAutomaton :: (JSON a, Ord a, Show a, Read a) => FilePath -> Automaton a b -> IO ()
saveAutomaton path = writeFile path . serialize

loadAutomaton :: (JSON a, Ord a, Show a, Read a) => FilePath -> IO (Automaton a b)
loadAutomaton path = deserialize <$> readFile path


isDeterministic :: Ord a => Automaton a b -> Bool
isDeterministic aut = singleInitialState &&
                      onlyDeterministicTransitions &&
                      noMissingTransitions &&
                      noEpsilonTransitions
  where
    singleInitialState = 1 == length (initialStates aut)
    hasSingleSuccessorState = (1 ==) . length
    onlyDeterministicTransitions = all hasSingleSuccessorState $ transitions aut
    noMissingTransitions = all (`Map.member` transitions aut)
                                [(st,S sym) | st <- toList $ states aut,
                                            sym <- toList $ alphabet aut]
    noEpsilonTransitions = isEpsilonFree aut


isEpsilonFree :: Ord a => Automaton a b -> Bool
isEpsilonFree = notElem Eps . map snd . Map.keys . transitions


nfa2dfa :: Ord a => Automaton a b -> Automaton a b
nfa2dfa aut = Automaton (Set.map rename newStates)
                          (alphabet aut)
                          (Set.map rename newInital)
                          (Set.map rename newFinals)
                          (Map.map (Set.singleton . rename) $ Map.mapKeys (first rename) newTransitions)
  where
    newStates = Map.foldrWithKey (\(q1,_) q2 res -> Set.fromList [q1,q2] `Set.union` res) Set.empty newTransitions
    newInital = Set.singleton $ epsSuccessors aut $ initialStates aut
    newFinals = Set.filter (any (`Set.member` finalStates aut)) newStates
    newTransitions = Map.mapKeys (S <$>) $ go Map.empty newInital

    renameingMap :: Map (Set State) State
    renameingMap = (Map.unions . zipWith (\sts -> Map.singleton sts . St) (toList newStates)) [0..]

    rename = (Map.!) renameingMap

    go ts sts | Set.null sts  = ts
              | otherwise     = let (st, stss) = Set.deleteFindMin sts
                                    newMappings = [((st,a), successorStates aut (S a) st) | a <- toList $ alphabet aut]
                                    updatedMapping = insertFromList ts newMappings
                                    newStateSets = Set.unions [Set.singleton succs |
                                                          (_,succs) <- newMappings, not $ Map.member succs $ Map.mapKeys fst updatedMapping] in
                                  go  updatedMapping
                                      (Set.union stss newStateSets)

insertFromList :: Ord k => Map k v -> [(k,v)] -> Map k v
insertFromList = foldr (uncurry Map.insert)


successorStates :: Ord a => Automaton a b -> Symbol a -> Set State -> Set State
successorStates aut a = foldMap (epsSuccessors aut . Set.singleton) . foldMap directSucc . epsSuccessors aut
  where
    directSucc st' = (fromMaybe Set.empty . Map.lookup (st', a) . transitions) aut


epsSuccessors :: Ord a => Automaton a b -> Set State -> Set State
epsSuccessors aut sts | sts == succs  = sts
                      | otherwise     = succs `Set.union` epsSuccessors aut succs
  where
    succs = foldMap (\st -> (Set.insert st . fromMaybe Set.empty . Map.lookup (st, Eps) . transitions) aut) sts


mapAutomaton :: Ord b => (a -> b) -> Automaton a c -> Automaton b c
mapAutomaton f (Automaton sts alph inits finals trans) = Automaton  sts
                                                            (Set.map f alph)
                                                            inits
                                                            finals
                                                            (Map.mapKeys ((f <$>) <$>) trans)


replaceAlphabet :: (Ord a, Ord b) => Set b -> Automaton a c -> Maybe (Automaton b c)
replaceAlphabet newAlph aut | Set.size newAlph == Set.size (alphabet aut) = Just $ mapAutomaton
                                                                                    ((`Set.elemAt` newAlph) . (`Set.findIndex` alphabet aut))
                                                                                    aut
                            | otherwise                                   = Nothing
