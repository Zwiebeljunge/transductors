{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE RankNTypes            #-}

module FST.Utils
    ( fsa2fst
    , fst2fsa
    , (<.>)
    , trimTrans
    , transDropOutput
    ) where

import           Control.Arrow          (first)
import           Data.Foldable          (toList)
import qualified Data.Map.Strict        as Map (assocs, filterWithKey,
                                                fromListWith, fromListWith, map,
                                                mapKeys, mapWithKey)
import           Data.Maybe             (fromMaybe)
import qualified Data.Set               as Set (filter, foldr, fromList,
                                                lookupIndex, map, member,
                                                singleton, union)
import           Lens.Micro             ((%~), (&))


import           FST.AutomatonFunctions (nfa2dfa)
import           FST.AutomatonTypes     (Automaton (Automaton))
import qualified FST.AutomatonTypes     as FSA (Automaton (..))
import           FST.TransductorTypes   (State (..), Transductor (Transductor),
                                         finalStatesL, initialStatesL, statesL,
                                         transitionsL)
import qualified FST.TransductorTypes   as FST (Transductor (..), associations)
import           FST.Types              (AppError, Error (..))



fsa2fst :: Ord a => Automaton a b -> Transductor a a
fsa2fst aut = Transductor (FSA.states aut)
                              (FSA.alphabet aut)
                              (FSA.alphabet aut)
                              (FSA.initialStates aut)
                              (FSA.finalStates aut)
                              (Map.mapWithKey (\(_,sym) -> Set.map ((,) sym)) $
                                        FSA.transitions aut)

fst2fsa :: Ord b => Transductor a b -> Automaton b c
fst2fsa trans = Automaton
                    (FST.states trans)
                    (FST.outputAlphabet trans)
                    (FST.initialStates trans)
                    (FST.finalStates trans)
                    (Map.fromListWith Set.union $
                          concatMap (\((st1,_),symSts) -> [((st1,sym),Set.singleton st2) | (sym,st2) <- toList symSts]) $
                          Map.assocs $
                          FST.transitions trans)



class Composable f g a b c where
  compose :: f a b -> g b c -> AppError (g a c)

(<.>) :: Composable f g a b c => g b c -> f a b -> AppError (g a c)
(<.>) = flip compose


instance (Eq b, Ord a, Ord c) => Composable Transductor Transductor a b c where
  compose t1 t2 |  FST.outputAlphabet t1 == FST.inputAlphabet t2 =
                        Right $ trimTrans $ Transductor states
                                  inputAlphabet
                                  outputAlphabet
                                  initialStates
                                  finalStates
                                  transitions
                | otherwise = Left CompositionError
    where
      states          = stateCartesianProduct FST.states FST.states
      inputAlphabet   = FST.inputAlphabet t1
      outputAlphabet  = FST.outputAlphabet t2
      initialStates   = stateCartesianProduct FST.initialStates FST.initialStates
      finalStates     = stateCartesianProduct FST.finalStates FST.finalStates
      transitions     = newTransitions (FST.transitions t1) (FST.transitions t2)

      stateCartesianProduct getStates1 getStates2 = renameStates $
                                              getStates1 t1 >< getStates2 t2

      newTransitions tr1 tr2 = Map.mapKeys (first newStateName) $
                                Map.map (Set.map (newStateName <$>)) $
                                Map.fromListWith Set.union [(((q1,q2),x),Set.singleton (z,(q1',q2'))) |
                                                                      ((q1,x),(y,q1')) <- FST.associations tr1,
                                                                      ((q2,y'),(z,q2')) <- FST.associations tr2,
                                                                      y == y']

      renameStates = Set.map newStateName

      newStateName (St x,St y) = St $ x * (toInteger . length . FST.states) t2 + y

      s1 >< s2 = Set.fromList [(a,b) | a <- toList s1, b <- toList s2]

instance (Eq a, Ord a) => Composable Transductor Automaton a a c where
  compose trans = (nfa2dfa . fst2fsa <$>) . (trans <.>) . fsa2fst


trimTrans :: (Ord a, Ord b) => Transductor a b -> Transductor a b
trimTrans = renameSequential . converge transEquiv removeUnreachableStates
  where
    transEquiv :: Transductor a b -> Transductor a b -> Bool
    transEquiv tr1 tr2 = FST.states tr1 == FST.states tr2

    removeUnreachableStates :: Transductor a b -> Transductor a b
    removeUnreachableStates trans = Set.foldr removeState trans $
                                    Set.filter (not . hasStateIncomingTransition trans) $
                                    FST.states trans

    removeState :: State -> Transductor a b -> Transductor a b
    removeState st trans = trans  & statesL %~ Set.filter (/= st)
                                  & initialStatesL %~ Set.filter (/= st)
                                  & finalStatesL %~ Set.filter (/= st)
                                  & transitionsL %~ Map.filterWithKey (\(kSt,_) _ -> kSt /= st)

    hasStateIncomingTransition :: Transductor a b -> State ->  Bool
    hasStateIncomingTransition trans st = (st `Set.member` FST.initialStates trans) ||
                                            any (any ((== st) . snd)) (Map.filterWithKey (\(kSt,_) _ -> kSt /= st) $ FST.transitions trans)

    converge :: (a -> a -> Bool) -> (a -> a) -> a -> a
    converge p = until =<< (p =<<)

    renameSequential :: (Ord a, Ord b) => Transductor a b -> Transductor a b
    renameSequential trans = trans  & statesL %~ Set.map renamingFunc
                                    & initialStatesL %~ Set.map renamingFunc
                                    & finalStatesL %~ Set.map renamingFunc
                                    & transitionsL %~ Map.map (Set.map (renamingFunc <$>))
                                    & transitionsL %~ Map.mapKeys (first renamingFunc)
      where
        renamingFunc = computeRenaming trans

    computeRenaming :: Transductor a b -> State -> State
    computeRenaming trans st = fromMaybe st $ (St . toInteger <$>) $ Set.lookupIndex st $ FST.states trans


transDropOutput :: Ord a => Transductor a b -> Automaton a c
transDropOutput trans = Automaton
                    (FST.states trans)
                    (FST.inputAlphabet trans)
                    (FST.initialStates trans)
                    (FST.finalStates trans)
                    (Map.map (Set.map snd) $
                          FST.transitions trans)
