{-# LANGUAGE OverloadedStrings #-}

module FST.AutomatonTypes
    ( Automaton (..)
    , AmoreAutomaton
    , mkAutomaton
    ) where

import           Data.Foldable          (toList)
import           Data.Map.Strict        (Map)
import qualified Data.Map.Strict        as Map (fromList, fromListWith, keys,
                                                lookup)
import           Data.Maybe             (fromMaybe, maybe)
import           Data.Set               (Set)
import qualified Data.Set               as Set (fromList, singleton, union)
import           Text.PrettyPrint.Boxes (Box, bottom, emptyBox, hsep, left,
                                         punctuateH, render, text, top, vsep,
                                         (/+/), (//), (<>))

import           FST.Json               (JSON (fromJSON, toJSON),
                                         JsValue (JsObject), toJsObject)
import           FST.TransductorTypes   (State, Symbol (..))


type ATransitionTable a = Map (State,Symbol a) (Set State)

data Automaton a b = Automaton {
                                states        :: Set State,
                                alphabet      :: Set a,
                                initialStates :: Set State,
                                finalStates   :: Set State,
                                transitions   :: ATransitionTable a
                              }

type AmoreAutomaton = Automaton Char ()

instance (JSON a, Ord a, Show a, Read a) => JSON (Automaton a b) where
  toJSON trans = toJsObject $ Map.fromList [  ("states", sts)
                                            , ("alphabet", alp)
                                            , ("initialStates", initials)
                                            , ("finalStates", finals)
                                            , ("transitions", trs)]
    where
      sts = toJSON $ states trans
      alp = toJSON $ alphabet trans
      initials = toJSON $ initialStates trans
      finals = toJSON $ finalStates trans
      trs = toJSON $ transitions trans
  fromJSON (JsObject objMap) = Automaton  sts
                                          alp
                                          initals
                                          finals
                                          trs
    where
      sts = fromJSON $ "states" #> objMap
      alp = fromJSON $ "alphabet" #> objMap
      initals = fromJSON $ "initialStates" #> objMap
      finals = fromJSON $ "finalStates" #> objMap
      trs = fromJSON $ "transitions" #> objMap

      (#>) key = fromMaybe (error $ "could not find key: " ++ show key) .
                    Map.lookup key

  fromJSON x = error $ "cannot convert " ++ show x ++ " to transductor"

mkAutomaton ::  Ord a =>
                  [State] ->
                  [a] ->
                  [State] ->
                  [State] ->
                  [(State,Symbol a,State)] ->
                  Automaton a b
mkAutomaton sts inp initS finS aut =
                      Automaton (Set.fromList sts)
                                (Set.fromList inp)
                                (Set.fromList initS)
                                (Set.fromList finS)
                                (Map.fromListWith Set.union $
                                    map (\(qi,x,qj) -> ((qi, x), Set.singleton qj)) aut)



instance (Ord a, Show a) => Show (Automaton a b) where
  show = render . ppTransductor

ppTransductor :: (Ord a, Show a) => Automaton a b -> Box
ppTransductor aut = header /+/
                      sts //
                      input //
                      inital //
                      final /+/
                      trs
  where
    header      = text "Automaton"
    sts         = text "States: " <> showSet states
    input       = text "Input: "  <> showSet alphabet
    inital      = text "Inital: " <> showSet initialStates
    final       = text "Final: "  <> showSet finalStates
    trs         = text "Transition Table" /+/ transTable

    showSet get = setBox $ get aut

    setBox :: (Foldable t, Show a) => t a -> Box
    setBox = punctuateH top (text ",") . map tShow . toList

    transTable :: Box
    transTable = hsep 1 bottom $ firstCol:[tShow a /+/ (unitBox <> tableColumn a) | a <- map S (toList $ alphabet aut) ++
                                                                                          [Eps | hasEpsilon aut]]


    firstCol = vsep 1 left [tShow st | st <- toList $ states aut]

    tableColumn a = vsep 1 left [targetBox st a | st <- toList $ states aut]
    targetBox st a = maybe  unitBox
                            setBox
                            (Map.lookup (st,a) trans)

    unitBox = emptyBox 1 1

    tShow :: Show a => a -> Box
    tShow = text . show

    trans = transitions aut

    hasEpsilon :: Ord a => Automaton a b -> Bool
    hasEpsilon = elem Eps . map snd . Map.keys . transitions
