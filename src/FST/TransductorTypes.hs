{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell   #-}

module FST.TransductorTypes
    ( State (..)
    , Symbol (..)
    , Transductor (..)
    , mkTransductor
    , associations
    , statesL
    , inputAlphabetL
    , outputAlphabetL
    , initialStatesL
    , finalStatesL
    , transitionsL
    ) where

import           Data.Foldable          (toList)
import           Data.List              (intersperse)
import           Data.Map.Strict        (Map)
import qualified Data.Map.Strict        as Map (assocs, foldMapWithKey,
                                                fromList, fromListWith, lookup,
                                                map)
import           Data.Maybe             (fromMaybe)
import           Data.Set               (Set)
import qualified Data.Set               as Set (fromList, singleton, union)
import           Lens.Micro.TH          (makeLensesFor)
import           Text.PrettyPrint.Boxes (Box, hcat, left, render, text, top,
                                         vcat, (/+/), (//), (<+>), (<>))

import           FST.Json               (JSON (fromJSON, toJSON),
                                         JsValue (JsObject, JsString),
                                         NString (NString), toJsObject)



newtype State = St { unState :: Integer }
  deriving (Ord, Eq)

instance JSON State where
  toJSON = toJSON . unState
  fromJSON = St . fromJSON

data Symbol a = S a | Eps
  deriving (Eq)

instance Show State where
  show (St x) = show x

instance Show a => Show (Symbol a) where
  show Eps   = "eps"
  show (S s) = show s

instance Ord a => Ord (Symbol a) where
  compare Eps   Eps   = EQ
  compare Eps   _     = LT
  compare _     Eps   = GT
  compare (S x) (S y) = compare x y

instance (Show a, Read a) => JSON (Symbol a) where
  toJSON = toJSON . NString . show
  fromJSON (JsString "eps") = Eps
  fromJSON (JsString c)     = S $ read c
  fromJSON x                = error $ "cannot convert " ++ show x ++ " to symbol"


instance Functor Symbol where
  fmap _ Eps   = Eps
  fmap f (S x) = S $ f x

type TTransitionTable a b = Map (State,Symbol a) (Set (Symbol b, State))

data Transductor a b = Transductor {
                                  states         :: Set State,
                                  inputAlphabet  :: Set a,
                                  outputAlphabet :: Set b,
                                  initialStates  :: Set State,
                                  finalStates    :: Set State,
                                  transitions    :: TTransitionTable a b
                                }

makeLensesFor [ ("states", "statesL")
              , ("inputAlphabet", "inputAlphabetL")
              , ("outputAlphabet", "outputAlphabetL")
              , ("initialStates", "initialStatesL")
              , ("finalStates", "finalStatesL")
              , ("transitions", "transitionsL")] ''Transductor

associations :: TTransitionTable a b -> [((State,Symbol a),(Symbol b, State))]
associations = Map.foldMapWithKey (\k -> foldMap (\v -> [(k,v)]))


instance (JSON a, JSON b, Ord a, Ord b, Show a, Show b, Read a, Read b) => JSON (Transductor a b) where
  toJSON trans = toJsObject $ Map.fromList [  ("states", sts)
                                            , ("inputAlphabet", inp)
                                            , ("outputAlphabet", out)
                                            , ("initialStates", initials)
                                            , ("finalStates", finals)
                                            , ("transitions", trs)]
    where
      sts = toJSON $ states trans
      inp = toJSON $ inputAlphabet trans
      out = toJSON $ outputAlphabet trans
      initials = toJSON $ initialStates trans
      finals = toJSON $ finalStates trans
      trs = toJSON $ transitions trans
  fromJSON (JsObject objMap) = Transductor  sts
                                          inp
                                          out
                                          initals
                                          finals
                                          trs
    where
      sts = fromJSON $ "states" #> objMap
      inp = fromJSON $ "inputAlphabet" #> objMap
      out = fromJSON $ "outputAlphabet" #> objMap
      initals = fromJSON $ "initialStates" #> objMap
      finals = fromJSON $ "finalStates" #> objMap
      trs = fromJSON $ "transitions" #> objMap


      (#>) key = fromMaybe (error $ "could not find key: " ++ show key) .
                    Map.lookup key

  fromJSON x = error $ "cannot convert " ++ show x ++ " to transductor"

mkTransductor ::  (Ord a, Ord b) => [State]
                        -> [a]
                        -> [b]
                        -> [State]
                        -> [State]
                        -> [(State,Symbol a,Symbol b,State)]
                        -> Transductor a b
mkTransductor sts inp outp initS finalS trans =
                      Transductor   (Set.fromList sts)
                                    (Set.fromList inp)
                                    (Set.fromList outp)
                                    (Set.fromList initS)
                                    (Set.fromList finalS)
                                    (Map.fromListWith Set.union $
                                        map (\(qi,x,y,qj) -> ((qi,x),Set.singleton (y,qj))) trans)

instance (Show a, Show b) => Show (Transductor a b) where
  show = render . ppTransductor

ppTransductor :: (Show a, Show b) => Transductor a b -> Box
ppTransductor trans = header /+/
                      sts //
                      input //
                      output //
                      inital //
                      final /+/
                      trs
  where
    header      = text "Transducer"
    sts         = text "States: " <> showSet states
    input       = text "Input: "  <> showSet inputAlphabet
    output      = text "Output: " <> showSet outputAlphabet
    inital      = text "Inital: " <> showSet initialStates
    final       = text "Final: "  <> showSet finalStates
    trs         = text "Transition Table" /+/ transRep

    showSet get = hcat top (intersperse (text ",") $ map (text . show) $ toList $ get trans)

    transRep = vcat left $ map transitionBox $ flatten $ Map.assocs $ Map.map toList $ transitions trans

    flatten = concatMap (\((qi,a),qjbs) -> [(qi,a,b,qj) | (b,qj) <- qjbs])

    transitionBox :: (Show a, Show b) => (State,Symbol a,Symbol b,State) -> Box
    transitionBox (v,w,x,y) = tShow v <+> tShow y <+> tShow w <> text ":" <> tShow x
      where
        tShow :: Show a => a -> Box
        tShow = text . show
