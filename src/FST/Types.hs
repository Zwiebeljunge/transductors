module FST.Types
    ( AppError
    , Error (..)
    ) where


data Error = CompositionError | ParserError String
  deriving (Show, Eq)

type AppError a = Either Error a
