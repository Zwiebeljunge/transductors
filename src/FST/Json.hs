module FST.Json
    ( JsValue (..)
    , JSON (..)
    , NString (..)
    , serialize
    , deserialize
    , serializeJSON
    , toJsObject
    ) where

import           Data.Foldable                 (toList)
import           Data.List                     (intercalate)
import           Data.Map.Strict               (Map)
import qualified Data.Map.Strict               as M (assocs, fromList, map)
import           Data.Set                      (Set)
import qualified Data.Set                      as S (fromList)
import           Text.ParserCombinators.Parsec (Parser, char, digit, many,
                                                many1, noneOf, parse, sepBy,
                                                (<|>))

data JsValue = JsString String | JsNumber Integer | JsArray [JsValue] | JsObject (Map String JsValue)
  deriving (Show)

serialize :: JSON a => a -> String
serialize = serializeJSON . toJSON

deserialize :: JSON a => String -> a
deserialize = fromJSON . deserializeJSON


serializeJSON :: JsValue -> String
serializeJSON (JsString str) = "\"" ++ str ++ "\""
serializeJSON (JsNumber n)   = show n
serializeJSON (JsArray xs)   = "[" ++ intercalate "," (map serializeJSON xs) ++ "]"
serializeJSON (JsObject xs)  = "{" ++ concat (intercalate [","] $ map (\(x,y) -> [  serializeJSON $ toJSON $ NString x
                                                                                  , ":"
                                                                                  , serializeJSON y]) $ M.assocs xs) ++ "}"

deserializeJSON :: String -> JsValue
deserializeJSON inp = case parse parseJsValue "json parser" inp of
                        Left msg  -> error $ show msg
                        Right val -> val


parseJsValue :: Parser JsValue
parseJsValue =  parseJsString <|>
                parseJsNumber <|>
                parseJsArray <|>
                parseJsObject


parseJsString :: Parser JsValue
parseJsString = JsString <$> parseString

parseString :: Parser String
parseString = do
                  char '\"'
                  s <- many $ noneOf "\""
                  char '\"'
                  return  s

parseJsNumber :: Parser JsValue
parseJsNumber = JsNumber . read <$> many1 digit

parseJsArray :: Parser JsValue
parseJsArray = do
                char '['
                xs <- sepBy parseJsValue (char ',')
                char ']'
                return $ JsArray xs

parseJsObject  :: Parser JsValue
parseJsObject = do
                char '{'
                xs <- sepBy parseElement (char ',')
                char '}'
                return $ JsObject $ M.fromList xs
  where
    parseElement = do
                    l <- parseString
                    char ':'
                    v <- parseJsValue
                    return (l,v)

toJsObject :: JSON a => Map String a -> JsValue
toJsObject = JsObject . M.map toJSON

class JSON a where
  toJSON :: a -> JsValue
  fromJSON :: JsValue -> a

instance JSON JsValue where
  toJSON = id
  fromJSON = id

instance JSON Char where
  toJSON = toJSON . NString . (:[])
  fromJSON (JsString [c]) = c
  fromJSON x              = error $ "cannot convert " ++ show x ++ " to char"

instance JSON a => JSON [a] where
  toJSON = JsArray . map toJSON
  fromJSON (JsArray xs) = map fromJSON xs
  fromJSON x            = error $ "cannot convert " ++ show x ++ " to []"

instance (JSON a, Ord a) => JSON (Set a) where
  toJSON = JsArray . map toJSON . toList
  fromJSON (JsArray xs) = S.fromList $ map fromJSON xs
  fromJSON x            = error $ "cannot convert " ++ show x ++ " to set"

instance (JSON a, JSON b, Ord a) => JSON (Map a b) where
  toJSON = toJSON . M.assocs
  fromJSON (JsArray xs) = M.fromList $ map fromJSON xs
  fromJSON x            = error $ "cannot convert " ++ show x ++ " to map"

instance (JSON a, JSON b) => JSON (a,b) where
  toJSON (x,y) = JsArray [toJSON x , toJSON y]
  fromJSON (JsArray [x,y]) = (fromJSON x, fromJSON y)
  fromJSON x               = error $ "cannot convert " ++ show x ++ " to (,)"

newtype NString = NString { unpack :: String }
  deriving (Show, Eq, Ord)

instance JSON NString where
  toJSON = JsString . unpack
  fromJSON (JsString str) = NString str
  fromJSON x              = error $ "cannot convert " ++ show x ++ " to string"

instance JSON Integer where
  toJSON = JsNumber
  fromJSON (JsNumber n) = n
  fromJSON x            = error $ "cannot convert " ++ show x ++ " to integer"
