module BrickUtils
    ( setEditorContent
    , clearEditor
    , emptyEditor
    , whenNotEmpty
    , focusSkip
    , focusOn
    , isFocused
    , listGetElem
    , listDeleteSelected
    , listIndex
    , (<++>)
    , emptyList
    , listDrawElement
    , editContent
    , handleAlphabetEditorEvent
    , isLetterKey
    , dropLastDirectory
    , dropPath
    , readMaybe
    ) where

import           Brick.Focus          (FocusRing, focusGetCurrent, focusNext)
import           Brick.Types          (EventM, Widget)
import           Brick.Widgets.Center (hCenter)
import           Brick.Widgets.Core   (str)
import           Brick.Widgets.Edit   (Editor (editorName), applyEdit, editor,
                                       getEditContents)
import           Brick.Widgets.List   (List, listElements, listElementsL,
                                       listInsert, listRemove, listReverse,
                                       listSelectedL)
import           Data.Char            (isLetter)
import           Data.Foldable        (toList)
import           Data.Text.Zipper     (clearZipper, deleteChar, deleteChar,
                                       deletePrevChar, insertChar, insertMany,
                                       moveLeft, moveRight)
import           Graphics.Vty         (Event (EvKey),
                                       Key (KBS, KChar, KDel, KLeft, KRight))
import           Lens.Micro           (Getting, (^.))
import           System.FilePath      (joinPath, splitPath)

-- | Editor utils

setEditorContent :: Monoid t => t -> Editor t n -> Editor t n
setEditorContent cont = applyEdit (insertMany cont . clearZipper)

clearEditor :: Editor String n -> Editor String n
clearEditor = emptyEditor . editorName

isEditorEmpty :: Editor String n -> Bool
isEditorEmpty = null . concat . getEditContents

editContent :: Editor String n -> String
editContent = concat . getEditContents

emptyEditor :: n -> Editor String n
emptyEditor name = editor name (str . unlines) (Just 1) []

handleAlphabetEditorEvent :: (Eq t, Monoid t) => Event -> Editor t n -> EventM n (Editor t n)
handleAlphabetEditorEvent e ed =
        let f = case e of
                  EvKey KDel []             -> deleteChar . deleteChar
                  EvKey (KChar c) []        | c /= '\t' -> insertChar ',' . insertChar c
                  EvKey KLeft []            -> moveLeft . moveLeft
                  EvKey KRight []           -> moveRight . moveRight
                  EvKey KBS []              -> deletePrevChar . deletePrevChar
                  _                         -> id
        in return $ applyEdit f ed

-- | AppState utils

whenNotEmpty :: a -> (a -> a) -> Editor String n -> a
whenNotEmpty st f ed  | isEditorEmpty ed  = st
                      | otherwise         = f st


-- | Focus Utils

focusSkip :: Eq n => [n] -> (FocusRing n -> FocusRing n) -> FocusRing n -> FocusRing n
focusSkip ns focusFun = skip . focusFun
  where
    skip fr = case focusGetCurrent fr of
                Nothing   -> fr
                Just curr -> if curr `elem` ns  then focusSkip ns focusFun fr
                                                else fr

focusOn :: Eq n => n -> FocusRing n -> FocusRing n
focusOn name fr = case focusGetCurrent fr of
                    Nothing   -> fr
                    Just curr -> go curr name (focusNext fr)
  where
    go start target ring = let newFocus = focusGetCurrent ring in
                              if newFocus == Just start then fr
                                                        else
                                                          if newFocus == Just target  then ring
                                                                                      else go start target (focusNext ring)

isFocused :: Eq n => n -> FocusRing n -> Bool
isFocused name fr = case focusGetCurrent fr of
                      Just focused -> focused == name
                      Nothing      -> False


-- | List Utils

listGetElem :: Int -> List n e -> Maybe e
listGetElem i lst = (!!! i) $ toList $ lst ^. listElementsL
  where
    (x:_)  !!! 0 = Just x
    (_:xs) !!! j = xs !!! (j-1)
    _ !!! _      = Nothing

listDeleteSelected :: List n e -> List n e
listDeleteSelected xs = case xs ^. listSelectedL of
                        Just i  -> listRemove i xs
                        Nothing -> xs

listIndex :: (e -> Bool) -> List n e -> Maybe Int
listIndex p xs = foldr (\(i,x) z -> if p x then Just i else z) Nothing $ zip [0..] (toList $ listElements xs)

(<++>) :: e -> List n e -> List n e
x <++> xs = (listReverse . listInsert 0 x . listReverse) xs

emptyList :: Getting (List n a) s (List n a) -> s -> Bool
emptyList l st = null $ st ^. l ^. listElementsL

listDrawElement :: Show a => Bool -> a -> Widget n
listDrawElement _ = hCenter . str . show

-- | General Utils

isLetterKey :: Event -> Bool
isLetterKey (EvKey (KChar x) []) = isLetter x
isLetterKey _                    = False

dropLastDirectory :: FilePath -> FilePath
dropLastDirectory = joinPath . ("/" :) . init . splitPath

dropPath :: FilePath -> FilePath
dropPath = last . splitPath


readMaybe :: Read a => String -> Maybe a
readMaybe s = case reads s of
                [(x,"")] -> Just x
                _        -> Nothing
