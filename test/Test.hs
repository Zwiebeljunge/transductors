module Main
    ( main
    ) where

import           Test.QuickCheck.Poly
import           Test.Tasty
import           Test.Tasty.QuickCheck  as QC

import           FST.AutomatonFunctions

main :: IO ()
main = defaultMain tests

tests :: TestTree
tests = testGroup "automaton functions"
                  [ propIsDeterministic
                  ]


propIsDeterministic :: TestTree
propIsDeterministic = testGroup "determinism"
                                [ --propSingleInitialState
                                ]
  where
    propSingleInitialState = True--QC.testProperty "DFA with single initial state" $
                                              -- \auto -> isDeterministic (auto :: Automaton Char)
